# Users and Tournaments API Service

Project's documentation is [here](https://software-engineering-php.gitlab.io/documentation)
and its repository is [here](https://gitlab.com/software-engineering-php/documentation).
Documentation is written in Russian language and uses
[mdBook](https://github.com/rust-lang/mdBook).
