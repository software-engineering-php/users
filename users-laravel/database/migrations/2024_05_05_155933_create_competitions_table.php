<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCompetitionsTable extends Migration
{

    protected $table = 'competitions';
    public function up()
    {
        Schema::create('competitions', function (Blueprint $table){
            $table->id();
            $table->string('title');
            $table->dateTime('start_time');
            $table->dateTime('end_time');
            $table->timestamps();
        });

        Schema::create('competition_participants', function (Blueprint $table) {
            $table->id();
            $table->foreignId('competition_id')->constrained()->onDelete('cascade');
            $table->string('participant_nickname');
            $table->integer('points')->default(0);
            $table->timestamps();
        });
}

    public function down()
    {
        Schema::dropIfExists('competitions');
        Schema::dropIfExists('competition_participants');
    }
}
