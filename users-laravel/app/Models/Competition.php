<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Competition extends Model
{
    protected $fillable = ['title', 'start_time', 'end_time'];

    public function participants()
    {
        return $this->hasMany(CompetitionParticipant::class);
    }
}


