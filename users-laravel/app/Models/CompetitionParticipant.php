<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class CompetitionParticipant extends Model
{
    protected $fillable = ['participant_name', 'points'];

    public function competition()
    {
        return $this->belongsTo(Competition::class);
    }
}
