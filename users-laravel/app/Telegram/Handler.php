<?php

namespace App\Telegram;

use DefStudio\Telegraph\Handlers\WebhookHandler;
use Illuminate\Support\Facades\Http;
use Telegram\Bot\Objects\Update;
use DefStudio\Telegraph\Facades\Telegraph;
use DefStudio\Telegraph\Keyboard\Button;
use DefStudio\Telegraph\Keyboard\Keyboard;
use Stringable;

class Handler extends WebhookHandler
{

    public function start(): void
    {
        $this->reply("Hi!\nWelcome to your CodeWars Telegram bot.\nHere you can compete with your friends by solving CodeWars tasks.\nFirstly click /help to find out more.");
    }

    public function help(): void
    {
        $this->reply("There are some usage examples:\nEntry Competition => /entry a1ex(CodeWars nickname)\nShow users => /getusers\n");
    }

    public function getusers():void
    {
        $res = "Список участников:\n";
        $response = Http::post("http://95.164.8.171:8000/api/v1/service", [
            'state' => 'update',
        ]);
        $data = json_decode($response, true);

        $users = array();

        foreach ($data['users'] as $user) {
            $username = $user['username'];
            $deltaHonor = $user['honorDelta'];
            $users[$username] = $deltaHonor;
        }

        arsort($users);
        $place = 1;
        foreach ($users as $key => $value) {
            $t = "$place место: $key - $value\n";
            $res = $res . $t;
            $place = $place + 1;
        }

        $this->chat->html($res)->send();
    }

    public function enter($nickname):void
    {
        if ($nickname == '/enter')
        {
            $this->chat->html("<b>Никнейм не введен</b>")->send();
        }
        else {
            Http::post("http://95.164.8.171:8000/api/v1/users", [
                'username' => $nickname,
            ])->throw();
            $this->chat->message("Successful")->send();
        }
    }

    public function reset():void
    {
        $response = Http::post("http://95.164.8.171:8000/api/v1/service", [
            "state" => "reset",
        ]);

        $data = json_decode($response, true);
        if ($data["success"] === true) {
            $this->reply("Successful reset");
        }
    }

    public function startc():void
    {
        $res = "Список участников:\n";
        $response = Http::post("http://95.164.8.171:8000/api/v1/service", [
            'state' => 'start',
        ]);
        $data = json_decode($response, true);

        $users = array();

        foreach ($data['users'] as $user) {
            $username = $user['username'];
            $deltaHonor = $user['honorDelta'];
            $users[$username] = $deltaHonor;
        }

        arsort($users);
        $place = 1;
        foreach ($users as $key => $value) {
            $t = "$place место: $key - $value\n";
            $res = $res . $t;
            $place = $place + 1;
        }

        $this->chat->html($res)->send();
    }



}


